g_t = 1;
step_size = 9;
i_depth = 8;
o_depth = i_depth + 8;
screw_depth = 3;
outer_lip_thickness = 3;
length = 179;
slit_width = 20;
slit_length = 105;

module basic_shape(l, s_w, d, s_l) {
    difference() {
        cube([l, l, d], center = true);

        translate([s_l, 0, 0])
        union() {
            cube([s_l, s_w, d + 0.1], center = true);
            $fn=50; // make the circle more circle-y-er
            translate([-(s_l/2), 0, 0])
            cylinder(h = d + 0.1, r = (s_w/ 2), center = true);
        }
    }
}

module outer_shape(w, s_w, d, t, s_l) { // w = width, s_w = slit width, d = depth, t = thickness
    difference() {
        basic_shape(w, s_w, d, s_l);
        basic_shape(w-t, s_w+t, d+0.1, s_l);
    }
}

module grid(l, s_s, g_t, d) {
    rotate(45)
    for (x = [ -l: s_s: l ])
        for (y = [ -l: s_s: l])
            translate([x, y, 0]) cube([s_s - g_t, s_s - g_t, d + 0.1], center = true);

}

module screws() {
    translate([70, 45, -i_depth + screw_depth])
    cube([10,10,i_depth], center=true);

    translate([70, -45, -i_depth + screw_depth])
    cube([10,10,i_depth], center=true);

    translate([-63, 58, -i_depth + screw_depth])
    cube([10,10,i_depth], center=true);

    translate([-63, -58, -i_depth + screw_depth])
    cube([10,10,i_depth], center=true);
}

module plate() {
    difference() {
        union() {
            difference() {
                // overall shape with cutout
                basic_shape(length, slit_width, i_depth, slit_length);
                // lots of vertical cuboids to cut holes in the basic shape
                grid(length, step_size, g_t, i_depth);
            }

            // add the outer lip
            translate([0, 0, 0 - ((o_depth - i_depth) / 2)])
            outer_shape(length + outer_lip_thickness, slit_width, o_depth, outer_lip_thickness, slit_length);
        }

        // screw positions
        screws();
    }
}

plate();
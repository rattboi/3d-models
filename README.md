# 3d Models

## Glastar G3 Plate

## 3DS XL Grip with modifications

* Added clip to hold in 3DS better (can still fall out if you try hard and shake it upside down, but in general case it won't come out)
* Added hole to see charging LED

### Printing Instructions

* load stl, use 15% infill (or similar) for the model
* add a modification block around the clip, and use more infill (I used 80%). The clip is the weakest part and most likely to fail if not strong.


module ds_grip() {
    // center the imported model
    translate([-130,-130,0])
    import(file = "3ds_grip_stand_meshmixer.stl", convexity=10);
}

module light_hole() {
    $fn=150;
    translate(v = [53.5,-22, 20]) 
    rotate([90,0,0]) 
    cylinder(h = 10, r = 3);
}

module holding_clip() {
    h = 45;
    w = 30;
    e = 4;
    union() {
        cube([w, 3, h], center=true);

        translate(v = [0,-e/2,h/2+0.6]) 
        cube([w, e, 2], center=true);

        $fn=50;
        translate(v = [0,0,h/2+0.1]) 
        rotate([0, 90,0]) 
        cylinder(h = w, r = 1.5, center=true);

        translate(v = [0,-e,h/2+.6]) 
        rotate([0, 90,0]) 
        cylinder(h = w, r = 1, center=true);
    }
}

union() {
    difference() {
        ds_grip();
        light_hole();
    }

    translate(v = [25,6,80]) 
    rotate([-12.8,0,0])
    holding_clip();
}
